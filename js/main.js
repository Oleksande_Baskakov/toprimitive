Object.prototype[Symbol.toPrimitive] = function(hint) { 
    let result;
    console.log(hint);

    if (hint === 'string') {
        result = this.toString ? this.toString() : this;
        if (result === Object(result)) {
            result = result.valueOf ? result.valueOf() : result;
        }

    } else {
        result = this.valueOf ? this.valueOf() : this;
        if (result === Object(result)) {
            result = result.toString ? result.toString() : result;
        }
    }

    return result;
}

const obj = {
    name: 'Alex',
    age: '24'
}

//hint: string
console.log(String(obj));
alert(obj);
const newObj = {
    job: 'electric'
}
newObj[obj] = 123
console.log(newObj)
//hint: string

//hint: number
console.log(obj - 5);
console.log(+obj);
console.log(Number(obj));
//hint: number

//hint: default
console.log(obj + 10);
console.log(obj == 1);
console.log(obj == 'Vasya');
//hint: default